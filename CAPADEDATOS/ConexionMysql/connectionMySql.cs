﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAPADEDATOS.ConexionMysql
{
    public class connectionMySql
    {
        //Metodo para crear la cadena de conexion utilizando la clase "MySqlConnectionStringBuilder"
        private static string UseMySqlConnectionStrigBuilder()
        {
            MySqlConnectionStringBuilder _ConexionString = new MySqlConnectionStringBuilder()
            {
                Server = "localhost",
                //Port = 3400,
                Port = 3402,
                //Database = "pymecfdisma170426ui7",
                Database = "pymecfdiaaa010101aaa",
                UserID = "codemx",
                Password = "codemx",
                SslMode = none
                
            };
            return _ConexionString.ToString();
        }
        //Se crea la conexion con la clase "MySqlConnection"
        private static MySqlConnection _conexion = new MySqlConnection(UseMySqlConnectionStrigBuilder());
        private static MySqlSslMode none;

        public MySqlConnection OpeenMySqlConnection()
        {
            try
            {
                if (_conexion.State == ConnectionState.Closed)
                {
                    _conexion.Open();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error al abrir la conección  " + e,"Notificación",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            return _conexion;
        }

        public MySqlConnection CloseMySqlConnection()
        {
            try
            {
                if (_conexion.State == ConnectionState.Open)
                {
                    _conexion.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("No se puede cerrar la conexión  "+e, "Notificacion", MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            return _conexion;
        }
    }
    
}
