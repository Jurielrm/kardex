﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace CAPADEDATOS.ConexionToMySql
{
  public  class ConnetionMysql:IDisposable
    {
        //private static string UseMySqlConnectionStringBuilder()
        //{
        //    //    MySqlConnectionStringBuilder _conneString = new MySqlConnectionStringBuilder()
        //    //    {
        //    //        Server = "localhost",
        //    //        Port = 3400,
        //    //        Database = "pymecfdisma170426ui7",
        //    //        UserID = "codemx",
        //    //        Password = "codemx",
        //    //        SslMode = MySqlSslMode.None
        //    //    };
        //    //    return _conneString.ToString();
        //    //}
            private static string UseMySqlConnectionStringBuilder()
            {
                MySqlConnectionStringBuilder _conneString = new MySqlConnectionStringBuilder()
                {
                    Server = "localhost",
                    Port = 3402,
                    Database = "pymecfdilan8507268ia",
                    UserID = "ROOT",
                    Password = "123456",
                    SslMode = MySqlSslMode.None

                };
                return _conneString.ToString();
            }
        public ConnetionMysql() {
            Conexion = new MySqlConnection(UseMySqlConnectionStringBuilder());
            try
            {
                if (Conexion.State == ConnectionState.Closed)
                {
                    Conexion.Open();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error al abrir la conexión " + e, "Notificación", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public MySqlConnection Conexion { get; set; }

       

        

        public MySqlConnection CloseMySqlConnection()
        {
            try
            {
                if (Conexion.State == ConnectionState.Open)
                {
                    Conexion.Close();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("No se puede cerrar la conexión  "+e, "Notificacion", MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            return Conexion;
        }

        public void Dispose()
        {
            if (Conexion != null && Conexion.State == ConnectionState.Open)
                Conexion.Clone();
        }
    }
}
