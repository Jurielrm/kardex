﻿using CAPADEDATOS.ConexionToMySql;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAPADEDATOS.ConsultaMysql
{
    public class ConsultaDatosProducto
    {
        private ConnetionMysql _IsConectando = new ConnetionMysql();
        private MySqlDataReader _isReader;

        public MySqlDataReader VerHistorialProductos(int codigo, DateTime fecha1, DateTime fecha2)
        {
            try
            {
                string query = "SELECT c.'FechaExpedicion' AS Fecha, 'SALIDA INVENTARIO' AS Movimiento," +
                               "u.'Login' AS Usuario, CONCAT('Venta ', c.'IdComprobante') AS Concepto," +
                               "CONCAT(pv.'Codigo',' - ',pv.'Nombre') AS Producto," +
                               "pv.'TotalProductos' AS Cantidad, pv.'PrecioUnitario'," +
                               "pv.'PrecioCompra' AS Costo FROM productovendido pv INNER JOIN comprobante c" +
                               "ON c.'IdComprobante' = pv.'Comprobante' INNER JOIN usuario u ON " +
                               "c.'Usuario'=u.'IdPersona' WHERE pv.'Comprobante' > 0  AND " +
                               "pv.'Codigo'= '" + codigo + "' AND c.FechaExpedicion BETWEEN '" + fecha1 + "'" +
                               "AND '" + fecha2 + "' ) UNION( SELECT c.FechaCancelacion AS Fecha," +
                               "'ENTRADA INVENTARIO' AS Movimiento, u.'Login' AS Usuario, CONCAT(" +
                               "'Cancelacion de Venta ', c.'IdComprobante') AS Concepto, CONCAT(" +
                               "pv.'Codigo', '-', pv.'Nombre') AS Producto, pv.'TotalProductos' AS Cantidad," +
                               "pv.'PrecioUnitario', pv.'PrecioCompra' AS Costo FROM productovendido pv" +
                               "INNER JOIN comprobante c ON c.'IdComprobante' = pv.'Comprobante'" +
                               "INNER JOIN usuario u ON c.'Usuario'= u.'IdPersona' " +
                               "WHERE pv.'Comprobante' > 0 AND c.'IsCancelado'= TRUE AND " +
                               "pv.'Codigo'= '" + codigo + "'  AND c.FechaCancelacion " +
                               "BETWEEN '" + fecha1 + "' AND '" + fecha2 + "')";

                MySqlCommand _consulMySqlCommand = new MySqlCommand();
                _consulMySqlCommand.Connection = _IsConectando.OpeenMySqlConnection();
                _consulMySqlCommand.CommandText = query;
                _isReader = _consulMySqlCommand.ExecuteReader();
            }
            catch (MySqlException e)
            {
                MessageBox.Show("Error en la consulta " + e, "Notificación", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return _isReader;
        }
    }
}
