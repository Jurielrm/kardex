﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPADEDATOS.Clases
{
    public class Perfil
    {
        public long IdPerfil { get; set; }
        public string Nombre { get; set; }
    }
}
