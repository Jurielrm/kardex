﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPADEDATOS.Clases
{
    public class Sucursal
    {
        public long IdSucursal { set; get; }
        public string Nombre { set; get; }
        public string GetSucursal()
        {
            return Nombre;
        }
    }
}
