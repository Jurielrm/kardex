﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPADEDATOS.Clases
{
    public class Usuario
    {//id,Login,Password,CodigoRenovacionPassword,FechaRegistro,IsActivo,Perfil,Sucursal,TipoUsuario
        public long IdPersona { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string CodigoRenovacionPassword { get; set; }
        public DateTime FechaDeRegistro { get; set; }
        public bool IsActivo { get; set; }
        public long Sucursal { get; set; }
        public char TipoDeUsuario { get; set; }

        public Perfil Perfil { get; set; }
        public string GetUsuario()
        {
            return Login + Password;
        }
    }
}
