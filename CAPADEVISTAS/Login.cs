﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using CAPADENEGOCIO.EncriptarPassword;
using CAPADENEGOCIO.Entityes.ManagementUsuario;

namespace CAPADEVISTAS
{
    public partial class Login : Form
    {
        
        

        public Login()
        {
            InitializeComponent();
        }

        [DllImport("user32.Dll", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.Dll", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hwnd, int wmsg, int wparam, int lparam);

        private void txtUsser_Enter(object sender, EventArgs e)
        {
            if (txtUsser.Text == "USUARIO")
            {
                txtUsser.Text = "";
                txtUsser.ForeColor = Color.DimGray;
            }
        }

        private void txtUsser_Leave(object sender, EventArgs e)
        {
            if (txtUsser.Text == "")
            {
                txtUsser.Text = "USUARIO";
                txtUsser.ForeColor = Color.DimGray;
            }
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            if (txtPass.Text == "CONTRASEÑA")
            {
                txtPass.Text = "";
                txtPass.ForeColor = Color.DimGray;
                txtPass.UseSystemPasswordChar = true;
            }
        }
               
        private void txtPass_Leave(object sender, EventArgs e)
        {
            if (txtPass.Text == "")
            {
                txtPass.Text = "CONTRASEÑA";
                txtPass.ForeColor = Color.DimGray;
                txtPass.UseSystemPasswordChar = false;
            }
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        
        
        
         private void btnAcceder_Click(object sender, EventArgs e)
         {
           ManagementUsuario objlogerUsuPass = new ManagementUsuario();

            var Pass = txtPass.Text;
            var Usu = txtUsser.Text;

            
                if (Pass == txtPass.Text)
                {
                    //lblErrorCont.Visible = false;

                    if (txtUsser.Text == "USUARIO" || txtPass.Text == "CONTRASEÑA") return;
                    var usuario = objlogerUsuPass.IniciarSesion(Usu, Pass);
                    if (usuario != null)
                    {
                        Configuracion.Usuario = usuario;
                        this.Hide();
                        PanelPrincipal logeo = new PanelPrincipal();
                        logeo.Show();
                    }
                    else
                    {
                        lblErrorAcceso.Text = "Usuario o contraseña incorrectos, Vuelva a intentarlo";
                        lblErrorAcceso.Visible = true;
                        txtPass.Text = "";
                        txtPass_Leave(null, e);
                        txtUsser.Text = "";
                        txtUsser_Leave(null, e);
                    }
                }
            
        }
    }  
}
