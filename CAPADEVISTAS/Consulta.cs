﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CAPADENEGOCIO.Entityes.ManagementUsuario;
using MySql.Data.MySqlClient;

namespace CAPADEVISTAS
{
    public partial class Consulta : Form
    {
        List<string> lista;
        public Consulta()
        {
            InitializeComponent();
        }

        private void Consulta_Load(object sender, EventArgs e)
        {
            
            //List<Producto> listapro = CAPADENEGOCIO.ListaProducto();
            //GVDatos.DataSource = listapro;
            //GVDatos.DataBindings();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void LlenarLista()
        {
            lista = new List<string>
            {
                "Exacto",
                "Tinta",
                "Folder",
                "Navaja",
                "Sobre",
                "Exacto"
            };
        }


        //private void InsertarFilas()
        //{

        ////    GVDatos.Rows.Insert(0, "1", "EXACTO", "MAE 18 MM", "2017-05-28 16:39:24");
        ////    GVDatos.Rows.Insert(1, "2", "TINTA", "AZOR PROTECTOCHECK 12 GRS. NEGRO", "2017-07-11 16:39:24");
        ////    GVDatos.Rows.Insert(2, "3", "FOLDER", "FOLDER CARTULINA", "2017-07-28 16:39:24");
        ////    GVDatos.Rows.Insert(3, "4", "Navaja", "Para excacto grueso barrilito", "2017-10-18 16:39:26");
        ////    GVDatos.Rows.Insert(4, "5", "Sobre", "Sobre Troqueles p/cd blanco 12.7x12.7 cms", "2017-10-20 16:39:26");
        ////    GVDatos.Rows.Insert(5, "6", "MESA", "PARA GABINETE SIN CARRETERILLAS DE 318X683X736 MM COLOR ARENA", "2017-11-28 19:00:20");
        ////    GVDatos.Rows.Insert(6, "7", "CARTUCHO", "ORIGINAL HP 954 AMARILLA", "2017-12-12 11:16:14");
        ////    GVDatos.Rows.Insert(7, "8", "TECLADO", "LOGITECH K", "LOGITECH K");
        ////    GVDatos.Rows.Insert(8, "9", "RENTA", "DE ESTRUCTURA", "2018-01-22 12:38:04");
        ////    GVDatos.Rows.Insert(9, "10", "LONA", "COATZACOALCOS", "2018-01-31 11:04:59");
        ////    GVDatos.Rows.Insert(10, "11", "TONER", "HP 410A AMARILLO LASERJET", "2018-02-15 12:46:24");
        ////    GVDatos.Rows.Insert(11, "12", "DIPTICOS", "TAMAÑO MEDIA CARTA CON PROGRAMA DE LA FERIA", "2018-04-02 18:43:40");
        ////    GVDatos.Rows.Insert(12, "13", "GORRA DE MALLA", "CON IMPRESIÓN AL FRENTE", "2018-07-18 16:07:31");
        ////    return _lee;
        //}

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            GVDatos.DataSource = new ManagmentItems().GetKardex(txtBuscar.Text,dTPDe.Value,dTPA.Value);
            GVDatos.Refresh();
            //GVDatos.DataSource = CAPADENEGOCIO.HistorialProducto.ver(txtBuscar.text, dTPDe.ToString.Text, dTPA.ToString.Text);
            //GVDatos.DataBindings();
            /*LlenarLista();
            GVDatos.Rows.Clear();
            var lee = txtBuscar.Text;
            int i = 0;
            foreach (var x in lista)
            {
                if (x.Equals(lee))
                {
                    GVDatos.Rows.Insert(i, x);
                    i++;
                }
            }*/

        }

        protected override void WndProc(ref Message msj)
        {
            const int CoordenadaWFP = 0x84; //ibicacion de la parte derecha inferior del form
            const int DesIzquierda = 16;
            const int DesDerecha = 17;
            if (msj.Msg == CoordenadaWFP)
            {
                int x = (int)(msj.LParam.ToInt64() & 0xFFFF);
                int y = (int)((msj.LParam.ToInt64() & 0xFFFF0000) >> 16);
                Point CoordenadaArea = PointToClient(new Point(x, y));
                Size TamañoAreaForm = ClientSize;
                if (CoordenadaArea.X >= TamañoAreaForm.Width - 16 && CoordenadaArea.Y >= TamañoAreaForm.Height - 16 && TamañoAreaForm.Height >= 16)
                {
                    msj.Result = (IntPtr)(IsMirrored ? DesIzquierda : DesDerecha);
                    return;
                }
            }
            base.WndProc(ref msj);
        }
    }
}
