﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CAPADEDATOS.ConsultaMysql;
using CAPADEDATOS.Entityes.ManagementUsuario;
using CAPADENEGOCIO.EncriptarPassword;
using MySql.Data.MySqlClient;

namespace CAPADENEGOCIO.Login
{
    public class CUsuario
    {
        public CUsuario() {}
        Encriptacion EncriPassword;
        public string UsuarioLogger(string _usu, string _pass)
        {
            //if (_usu == "USUARIO" || _pass !=null) return "Usuario y Contraseña Invalido";
            EncriPassword = new Encriptacion();
            var _passwor = EncriPassword.generarClaveSHA1(_pass);
            var usuario = new ManagementUsuario().IniciarSesion(_usu, _passwor);
            if (usuario != null)
                usuario.GetUsuario();
            return usuario.ToString();
        }
     
      
    }
}
