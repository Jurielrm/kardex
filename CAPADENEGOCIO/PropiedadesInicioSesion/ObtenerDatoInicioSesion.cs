﻿using CAPADEDATOS.ConsultasMysql;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPADENEGOCIO.PropiedadesInicioSesion
{
    public class ObtenerDatoInicioSesion
    {
        //INSTANCIA A LA CAPADEDATOS DE LA CLASE "ConsultaDatosInicioSesion"
        private ConsultaDatosInicioSesion _ObjetoDatos = new ConsultaDatosInicioSesion();
        //VARIABLES CON SUS METODOS GET SET
       public string _Usuario { get; set; }
        public string _Password { get; set; }
        //METODOS GET SET
        //public string Usuario
        //{
        //    set { _Usuario = value; }
        //    get { return _Usuario; }
        //}
        //public string Password
        //{
        //    set { _Password = value; }
        //    get { return _Password; }
        //}
        public MySqlDataReader _InicioSesion()
        {
            MySqlDataReader _Logear;
            _Logear = _ObjetoDatos.IniciarSesion(_Usuario, _Password);
             
            return _Logear;
        }

    }
}
