(SELECT 
  c.`FechaExpedicion` AS Fecha,
  "SALIDA INVENTARIO" AS Movimiento,
  u.`Login` AS Usuario,
  CAST(CONCAT("Venta ", c.`IdComprobante`) AS CHAR) AS Concepto,
  CONCAT(pv.`Codigo`,"-",pv.`Nombre`) AS Producto,
  pv.`TotalProductos` AS Cantidad,
  pv.`PrecioUnitario`,
  pv.`PrecioCompra` AS Costo 
FROM
  productovendido pv 
  INNER JOIN comprobante c 
    ON c.`IdComprobante` = pv.`Comprobante` 
  INNER JOIN usuario u ON c.`Usuario`=u.`IdPersona`
WHERE pv.Codigo=@codigo AND pv.`Comprobante` > 0   AND c.FechaExpedicion BETWEEN @fechaInicial AND @fechaFinal )
UNION(
SELECT 
  c.FechaCancelacion AS Fecha,
  "ENTRADA INVENTARIO" AS Movimiento,
  u.`Login` AS Usuario,
  CAST(CONCAT("Cancelacion de Venta ", c.`IdComprobante`)AS CHAR)  AS Concepto,
  CONCAT(pv.`Codigo`,"-",pv.`Nombre`) AS Producto,
  pv.`TotalProductos` AS Cantidad,
  pv.`PrecioUnitario`,
  pv.`PrecioCompra` AS Costo 
FROM
  productovendido pv 
  INNER JOIN comprobante c 
    ON c.`IdComprobante` = pv.`Comprobante` 
  INNER JOIN usuario u ON c.`Usuario`=u.`IdPersona`
WHERE pv.Codigo=@codigo AND pv.`Comprobante` > 0 AND c.`IsCancelado`=TRUE  AND c.FechaCancelacion BETWEEN @fechaInicial AND @fechaFinal  )
UNION(
SELECT 
  d.FechaDevolucion AS Fecha,
  "ENTRADA INVENTARIO" AS Movimiento,
  u.`Login` AS Usuario,
  CAST(CONCAT("Devolución No.",d.IdDevolucion," de Venta No. " , c.`IdComprobante`)AS CHAR) AS Concepto,
  CONCAT(pd.`Codigo`,"-",pd.`Nombre`) AS Producto,
  pd.`TotalProductos` AS Cantidad,
  pd.`PrecioUnitario`,
  pd.`PrecioCompra` AS Costo 
FROM
  productodevuelto pd 
  INNER JOIN Devolucion d 
    ON d.`IdDevolucion` = pd.`devolucion` 
  INNER JOIN Remision r
	ON r.`IdComprobante` = d.`Remision`
	INNER JOIN Comprobante c
	ON c.`IdComprobante`= r.`IdComprobante`
  INNER JOIN usuario u ON c.`Usuario`=u.`IdPersona`
WHERE pd.Codigo=@codigo AND  d.`FechaDevolucion` BETWEEN @fechaInicial AND @fechaFinal)
UNION(
SELECT 
  m.Fecha AS Fecha,
  "SALIDA INVENTARIO" AS Movimiento,
  u.`Login` AS Usuario,
  CAST(CONCAT("Merma ", m.`IdMerma`)AS CHAR) AS Concepto,
  CONCAT(pm.`Codigo`,"-",pm.`Nombre`) AS Producto,
  pm.`TotalProductos` AS Cantidad,
  pm.`PrecioUnitario`,
  pm.`PrecioCompra` AS Costo 
FROM
  productomerma pm 
  INNER JOIN adm_merma m 
    ON m.`IdMerma` = pm.`Merma` 
  INNER JOIN usuario u ON m.`Usuario`=u.`IdPersona`
WHERE pm.Codigo=@codigo AND  m.`Fecha` BETWEEN @fechaInicial AND @fechaFinal);
