﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CAPADENEGOCIO.EncriptarPassword
{
   public class Encriptacion
    {
        //METODO PARA HASHEAR LA CONTRASEÑA
        public string generarClaveSHA1(string Password)
        {
            //MD5 md5 = MD5CryptoServiceProvider.Create();
            MD5 md5 = MD5.Create();//SE CREA UN OBJETO md5 A PARTIR DE LA CLASE MD5 UTILIZANDO SU METODO Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] result;
            StringBuilder sb = new StringBuilder();
            result = md5.ComputeHash(encoding.GetBytes(Password)); 
            for (int i = 0; i < result.Length; i++)
            {
                if (result[i] < 16)// Convertimos los valores en hexadecimal
                {
                    sb.Append("0");// cuando tiene una cifra hay que rellenarlo con cero
                }
                sb.Append(result[i].ToString("x"));
            }
            //e10adc3949ba59abbe56e057f20f883e
            //Devolvemos la cadena con el hash 
            return sb.ToString();
        }
    }
}
