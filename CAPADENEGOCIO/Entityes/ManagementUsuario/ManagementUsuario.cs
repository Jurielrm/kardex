﻿using CAPADEDATOS.Clases;
using CAPADEDATOS.ConexionToMySql;
using CAPADENEGOCIO.EncriptarPassword;
using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace CAPADENEGOCIO.Entityes.ManagementUsuario
{
    public class ManagementUsuario
    {
        public ManagementUsuario() { }
        ConnetionMysql conexion;
        Encriptacion cripto;

        public Usuario IniciarSesion(string Login, string Password)
        {
            cripto = new Encriptacion();
            Usuario usuario = null;
            try
            {

                conexion = new ConnetionMysql();
                var _Password = cripto.generarClaveSHA1(Password);


                string query = "SELECT * FROM usuario INNER JOIN perfil ON usuario.`Perfil`=perfil.`IdPerfil` WHERE  Login =@login AND Password =@password";

                MySqlDataAdapter _dataAdapter = new MySqlDataAdapter();
                var comando = new MySqlCommand(query, conexion.Conexion);
                comando.Parameters.AddWithValue("login", Login);
                comando.Parameters.AddWithValue("password", _Password);
                _dataAdapter.SelectCommand = comando;
                MySqlDataReader reader = _dataAdapter.SelectCommand.ExecuteReader();               
             
                    //while (reader.Read())
                if(reader.Read())
                {
                        usuario = _Binding(reader);
                        conexion.CloseMySqlConnection();
                    //break;
                    return usuario;

                }
                else if(usuario == null)
                {
                    conexion.CloseMySqlConnection();
                    return usuario;
                }
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error" + ex);
            }
            return usuario;
        }
      private Usuario _Binding (MySqlDataReader _read)
        {
            var usuario = new Usuario();
            usuario.IdPersona = _read.GetInt64(0);
            usuario.Login = _read.GetString(1);
            usuario.Password = _read.GetString(3);
            usuario.CodigoRenovacionPassword = _read.IsDBNull(5) ? "Default value when null" : _read.GetString(5);
            //usuario.FechaDeRegistro = _read.IsDBNull(7) ? new DateTime() : _read.GetDateTime(8);
            usuario.IsActivo = _read.GetBoolean(9);
            usuario.Sucursal = _read.GetInt64(11);
            usuario.TipoDeUsuario = _read.GetChar(12);
            usuario.Perfil = new Perfil();
            usuario.Perfil.Nombre = _read.GetString(15);
            return usuario;
        }
        
    }
}
