﻿using CAPADEDATOS.Clases;
using CAPADEDATOS.ConexionToMySql;
using MySql.Data.MySqlClient;
using System;
using System.Windows.Forms;

namespace CAPADENEGOCIO.Entityes.ManagementUsuario
{
    public class SucursalManagement
    {
        public SucursalManagement() { }
        ConnetionMysql _Session;
        public Sucursal ConsultaSucursal(string _sucu)
        {
            Sucursal sucursal = null;
            try
            {
                _Session = new ConnetionMysql();
                string query = "SELECT * FROM sucursal WHERE Nombre = '" + _sucu + "'";
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                adapter.SelectCommand = new MySqlCommand(query, _Session.Conexion);
                MySqlDataReader reader = adapter.SelectCommand.ExecuteReader();
                while (reader.Read())
                {
                    sucursal = _Binding(reader);
                    break;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error " + e);
            }
            return sucursal;
        }
        private Sucursal _Binding (MySqlDataReader reader)
        {
            var sucursal = new Sucursal();
            sucursal.IdSucursal = reader.GetInt64(0);
            sucursal.Nombre = reader.GetString(1);
            return sucursal;
        }
    }
}
