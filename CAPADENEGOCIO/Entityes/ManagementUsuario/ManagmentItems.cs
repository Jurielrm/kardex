﻿using CAPADEDATOS.ConexionToMySql;
using CAPADENEGOCIO.Properties;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPADENEGOCIO.Entityes.ManagementUsuario
{
    public class ManagmentItems
    {
        ConnetionMysql _conexion;
        public ManagmentItems()
        {
            _conexion = new ConnetionMysql();
        }
        public List<Kardex> GetKardex(string item, DateTime fechaInicial, DateTime fechaFinal)
        {
            List<Kardex> lista = new List<Kardex>();
            try
            {
                string query = Resources.script_kardex;
                MySqlDataAdapter adapter = new MySqlDataAdapter();
                var comando = new MySqlCommand(query, _conexion.Conexion);
                comando.Parameters.AddWithValue("codigo", item);
                comando.Parameters.AddWithValue("fechaInicial", fechaInicial);
                comando.Parameters.AddWithValue("fechaFinal", fechaFinal);
                adapter.SelectCommand = comando;
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                if (ds.Tables.Count > 0)
                {
                    lista = Binding(ds.Tables[0]);
                }    
            }
            catch (Exception ex)
            {
                
            }
            return lista;
        }

        public List<Kardex> Binding(DataTable dt)
        {
            var lista = new List<Kardex>();
            foreach(DataRow dr in dt.Rows)
            {
                lista.Add(new Kardex
                {
                    Fecha = DateTime.Parse(dr["Fecha"].ToString()),
                    Movimiento=dr["Movimiento"].ToString(),
                    Usuario= dr["Usuario"].ToString(),
                    Concepto= dr["Concepto"].ToString(),
                    Producto = dr["Producto"].ToString(),
                    Cantidad=decimal.Parse(dr["Cantidad"].ToString()),
                    PrecioUnitario = decimal.Parse(dr["PrecioUnitario"].ToString()),
                    Costo = decimal.Parse(dr["Costo"].ToString()),

                });
            }
            return lista;
        }

        public class Kardex
        {
            public DateTime Fecha { get; set; }
            public string Movimiento { get; set; }
            public string Usuario { get; set; }
            public string Concepto { get; set; }
            public string Producto { get; set; }
            public decimal Cantidad { get; set; }
            public decimal PrecioUnitario { get; set; }
            public decimal Costo { get; set; }
        }
    }
}
