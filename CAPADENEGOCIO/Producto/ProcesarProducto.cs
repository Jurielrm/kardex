﻿using CAPADEDATOS.ConsultaMysql;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CAPADENEGOCIO.Producto
{
    public class ProcesarProducto
    {
        public int Codigo { get; set; }
        public DateTime Fecha1 { get; set; }
        public DateTime Fecha2 { get; set; }
        
    //    private ConsultaDatosProducto _objConsulDatos = new ConsultaDatosProducto();
                
        public DataTable VerHistorialProductos()
        {
            MySqlDataReader _logger;
            DataTable dataTable = null;

            try
            {
                using (MySqlDataAdapter adapter = new MySqlDataAdapter())
                {
               //     _logger = _objConsulDatos.VerHistorialProductos(Codigo, Fecha1, Fecha2);
                    adapter.Fill(dataTable);
                    List<InfoProducto> listaProductos = dataTable.AsEnumerable().Select(x => new InfoProducto {
                                                                           Fecha = x.Field<DateTime>("Fecha"),
                                                                           Movimiento = x.Field<string>("Movimiento"),
                                                                           Usuario = x.Field<string>("Usuario"),
                                                                           Concepto = x.Field<string>("Concepto"),
                                                                           Producto = x.Field<string>("Producto"),
                                                                           Cantidad = x.Field<int>("Cantidad"),
                                                                           PrecioUnitario = x.Field<decimal>("PrecioUnitario"),
                                                                           Costo = x.Field<decimal>("Costo")
                                                                        }).ToList();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Error al consultar productos" + e, "Notificación", 
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            
            return dataTable;
        }
    }
}