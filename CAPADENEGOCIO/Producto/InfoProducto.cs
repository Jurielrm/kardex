﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CAPADENEGOCIO.Producto
{
    public class InfoProducto
    {
        public DateTime Fecha { get; set; }
        public string Movimiento { get; set; }
        public string Usuario { get; set; }
        public string Concepto { get; set; }
        public string Producto { get; set; }
        public int Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public decimal Costo { get; set; }
    }
}
